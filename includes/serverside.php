<?php

class WunravEmbedYoutubeLiveStreaming {

    public $plugin_slug = 'wunrav-youtube-live-embed';
    public $options; // options entered into wp-admin form

    public $query_string; // Address + Data to request

    public $embed_width = 800;
    public $embed_height = 450;
    public $live_video_id = null;
    
    public function __construct() {

        register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

        add_shortcode( 'youtube-live', array($this, 'shortcode') );
        add_action( 'wp_head', array($this, 'load_scripts') );
        add_action( 'wp_head', array($this, 'alert') );
        add_action( 'admin_menu', array($this, 'admin_menu_init') );
        add_action( 'admin_init', array($this, 'admin_page_init') );
        add_filter( 'cron_schedules', array($this, 'add_wp_cron_schedule') );

        $this->options = get_option( $this->plugin_slug . '_settings' );

        $this->query_it(); // sets a cron to run the query

    }

    /**************************************************
     * Setup for Admin Page and Settings
     *************************************************/

    // add a menu item
    public function admin_menu_init()
    {
        add_options_page(
            'YouTube Auto Live Embed Settings',
            'YouTube Auto Live Embed',
            'manage_options',
            $this->plugin_slug,
            array( $this, 'admin_page_create' )
        );
    }
    

    // create the admin page layout
    public function admin_page_create()
    {  
        if ( ! current_user_can( 'manage_options' ) ) {
            wp_die( __( 'You do not have access to this page' ) );
        }

        // check that we have required fields before trying to query the API
        if ( ! isset( $this->options['alertTitle'] ) ||
             ! isset( $this->options['alertBtn'] ) ||
             ! isset( $this->options['alertBtnURL'] ) ||
             ! isset( $this->options['channelID'] ) ||
             ! isset( $this->options['apiKey'] ) ){

                 echo $this->admin_notice('Please fill in the required fields below and click the "Save Changes" button. The plugin will not work until all required fields are entered.');
        }

        echo '<div class="wrap">';
        echo '<h1>YouTube Auto Live Embed</h1>';
        echo '<p>To use this plugin, just place the <code>[youtube-live]</code> shortcode in the page or post you would like your live feed to appear. <a target="_blank" rel="noopener" href="https://gitlab.com/webunraveling/wunrav-youtube-live-embed">Instructions on setting up this plugin</a>.</p>';

        // notify user when testing is enabled
        if ( 1 <= $this->is_testing() ) {
            echo $this->admin_notice('<strong>NOTE:</strong> Your testing account '  . ( $this->is_testing() == 2 ? 'and debugging are both' : 'is' ) . ' enabled. Live videos on your main account will not appear until you disable the testing account.', 'notice-info');
        }
        echo '<form method="post" action="options.php">';
        settings_fields( $this->plugin_slug . '_settings' );
        do_settings_sections( $this->plugin_slug );
        submit_button();
        echo '</form>';
        echo '</div>';
    }


    // generate the admin options
    public function admin_page_init()
    {
        register_setting(
            $this->plugin_slug . '_settings', // option group
            $this->plugin_slug . '_settings', // option name
            array( $this, 'sanitize' )
        );

        /*****************************************
         * Form fields for verbage / customization
         ****************************************/
        add_settings_section(
            $this->plugin_slug . '-settings-customization', // section ID
            'Slideout / Notification', // section header name
            array($this, 'print_section_customization'), //callback
            $this->plugin_slug // page
        );

        add_settings_field(
            'alertTitle',
            'Header / Title <span class="required">*</span>',
            array($this, 'alertTitle_callback'),
            $this->plugin_slug, // page
            $this->plugin_slug . '-settings-customization' //section
        );

        add_settings_field(
            'alertMsg',
            'Message',
            array($this, 'alertMsg_callback'),
            $this->plugin_slug, //page
            $this->plugin_slug . '-settings-customization' //section
        );

        add_settings_field(
            'alertBtn',
            'Button Text <span class="required">*</span>',
            array($this, 'alertBtn_callback'),
            $this->plugin_slug, //page
            $this->plugin_slug . '-settings-customization' //section
        );

        add_settings_field(
            'alertBtnURL',
            'Button URL <span class="required">*</span>',
            array($this, 'alertBtnURL_callback'),
            $this->plugin_slug, //page
            $this->plugin_slug . '-settings-customization' //section
        );

        /*****************************************
         * Form fields for excluding a title
         ****************************************/
        add_settings_section(
            $this->plugin_slug . '-settings-filtering', // section ID
            'Filtering', // section header name
            array($this, 'print_section_filtering'), // callback
            $this->plugin_slug // page
        );

        add_settings_field(
            'filterType',
            'Filtering Type',
            array($this, 'filterType_callback'),
            $this->plugin_slug, // page
            $this->plugin_slug . '-settings-filtering' //section
        );


        add_settings_field(
            'filterList',
            'Titles to Filter',
            array($this, 'filtering_callback'),
            $this->plugin_slug, // page
            $this->plugin_slug . '-settings-filtering' //section
        );




        /*****************************************
         * Form fields for production account
         ****************************************/
        add_settings_section(
            $this->plugin_slug . '-settings-production', // section ID
            'Production Account', // section header name
            array($this, 'print_section_production'), // callback
            $this->plugin_slug // page
        );

        add_settings_field(
            'channelID',
            'Channel ID <span class="required">*</span>',
            array($this, 'channelID_callback'),
            $this->plugin_slug, // page
            $this->plugin_slug . '-settings-production' // section
        );

        add_settings_field(
            'apiKey', // ID (in form I think)
            'API Key <span class="required">*</span>',
            array($this, 'apiKey_callback'),
            $this->plugin_slug, // page
            $this->plugin_slug . '-settings-production' // section
        );

        /*****************************************
         * Form fields for TESTING account
         ****************************************/
        add_settings_section(
            $this->plugin_slug . '-settings-testing', // section ID
            'Testing Account', // section header name
            array($this, 'print_section_testing'), // callback
            $this->plugin_slug // page
        );

        add_settings_field(
            'testing-toggle',
            'Testing Account',
            array($this, 'testingToggle_callback'), // callback
            $this->plugin_slug,
            $this->plugin_slug . '-settings-testing' // section
        );

        add_settings_field(
            'debugging-toggle',
            'Debugging', // Title
            array($this, 'debuggingToggle_callback'), // callback
            $this->plugin_slug, // page
            $this->plugin_slug . '-settings-testing' // section
        );

        add_settings_field(
            'channelID-testing',
            'Channel ID',
            array($this, 'channelID_testing_callback'),
            $this->plugin_slug, // page
            $this->plugin_slug . '-settings-testing' // section
        );

        add_settings_field(
            'apiKey-testing',
            'API Key', // Title
            array($this, 'apiKey_testing_callback'), // callback
            $this->plugin_slug, // page
            $this->plugin_slug . '-settings-testing' // section
        );
    }




    /****************************************
     * Output sections
     ***************************************/
    public function print_section_customization()
    {
        // nothing to do here for now
    }


    public function print_section_production()
    {
        // nothing to do here for now
    }


    public function print_section_filtering()
    {
        echo '<p>You can setup a blacklist (streams to ignore), whitelist (streams to show) by entering one or more words found in the title of the live stream (i.e. Enter one word from the title, part of the title, or the entire title). An exact title (specific title to show) may also be entered (Note: if exact is chosen, the video will always appear, even after the live stream is over.</p>';
    }


    public function print_section_testing()
    {
        echo '<p><strong>NOTE:</strong> Use caution with debugging. It will show both your testing and production API keys.</p>';
    }


    // sanitize user input
    public function sanitize( $input )
    {
        $new_input = array();

        if ( isset($input['alertTitle']) ) {
            $new_input['alertTitle'] = sanitize_text_field($input['alertTitle']);
        }

        if ( isset($input['alertMsg']) ) {
            $new_input['alertMsg'] = sanitize_text_field($input['alertMsg']);
        }

        if ( isset($input['alertBtn']) ) {
            $new_input['alertBtn'] = sanitize_text_field($input['alertBtn']);
        }

        if ( isset($input['alertBtnURL']) ) {
            $new_input['alertBtnURL'] = esc_url_raw($input['alertBtnURL'], array('http','https'));
        }

        if ( isset($input['filterType']) ) {
            $new_input['filterType'] = wp_strip_all_tags($input['filterType']);
        }

        if ( isset($input['filterList']) ) {
            $new_input['filterList'] = sanitize_textarea_field($input['filterList']);
        }

        if ( isset($input['channelID']) ) {
            $new_input['channelID'] = sanitize_text_field($input['channelID']);
        }

        if ( isset($input['apiKey']) ) {
            $new_input['apiKey'] = sanitize_text_field($input['apiKey']);
        }
        
        if ( isset($input['testing-toggle']) ) {
            $new_input['testing-toggle'] = filter_var($input['testing-toggle'], FILTER_VALIDATE_BOOLEAN);
        }

        if ( isset($input['debugging-toggle']) ) {
            $new_input['debugging-toggle'] = filter_var($input['debugging-toggle'], FILTER_VALIDATE_BOOLEAN);
        }

        if ( isset($input['channelID-testing']) ) {
            $new_input['channelID-testing'] = sanitize_text_field($input['channelID-testing']);
        }

        if ( isset($input['apiKey-testing']) ) {
            $new_input['apiKey-testing'] = sanitize_text_field($input['apiKey-testing']);
        }

        return $new_input;
    }






    /****************************************
     * Callbacks for form fields
     ***************************************/
    public function alertTitle_callback() {
        printf(
            '<input type="text" id="alertTitle" name="' . $this->plugin_slug . '_settings[alertTitle]" value="%s" size="60" maxlength="500" required />',
            isset( $this->options['alertTitle'] ) ? esc_attr( $this->options['alertTitle']) : ''
        );
    }




    public function alertMsg_callback() {
        printf(
            '<textarea id="alertMsg" name="' . $this->plugin_slug . '_settings[alertMsg]" cols="65" rows="3" maxlength="800">%s</textarea>',
            isset( $this->options['alertMsg'] ) ? esc_attr( $this->options['alertMsg']) : ''
        );
    }




    public function alertBtn_callback() {
        printf(
            '<input type="text" id="alertBtn" name="' . $this->plugin_slug . '_settings[alertBtn]" value="%s" size="60" maxlength="500" required />',
            isset( $this->options['alertBtn'] ) ? esc_attr( $this->options['alertBtn']) : ''
        );
    }




    public function alertBtnURL_callback() {
        printf(
            '<input type="text" id="alertBtnURL" name="' . $this->plugin_slug . '_settings[alertBtnURL]" value="%s" size="60" maxlength="800" placeholder="must start with http:// or https://" required />',
            isset( $this->options['alertBtnURL'] ) ? esc_attr( $this->options['alertBtnURL']) : ''
        );
    }




    public function filterType_callback() {
        $selectedType = ( isset($this->options['filterType']) ? $this->options['filterType'] : 'Disable' );
        $filterTypes = array('Disable','Blacklist','Whitelist','Exact');

        foreach ( $filterTypes as $type ){
            printf('<input type="radio" value="%1$s" name="%2$s" %3$s /><label %4$s for="%1$s">%1$s</label> ',
                $type,
                $this->plugin_slug . '_settings[filterType]',
                checked( $type == $selectedType, 1, false ), // mark the radio selected
                ( $type == $selectedType ? 'style="font-weight:bold;"' : '' ) // bold text if selected
            );
        }
    }




    public function filtering_callback() {
        printf(
            '<textarea id="filterList" name="' . $this->plugin_slug . '_settings[filterList]" cols="65" rows="3">%s</textarea>',
            isset( $this->options['filterList']) ? esc_textarea( $this->options['filterList']) : ''
        );
    }




    public function channelID_callback() {
        printf(
            '<input type="text" id="channelID" name="' . $this->plugin_slug . '_settings[channelID]" value="%s" size="60" maxlength="24" required />',
            isset( $this->options['channelID'] ) ? esc_attr( $this->options['channelID']) : ''
        );
    }




    public function apiKey_callback() {
        printf(
            '<input type="text" id="apiKey" name="' . $this->plugin_slug .'_settings[apiKey]" value="%s" size="60" maxlength="39" required />',
            isset( $this->options['apiKey'] ) ? esc_attr( $this->options['apiKey']) : ''
        );
    }




    public function testingToggle_callback() {
        printf(
            '<input type="checkbox" id="testing-toggle" name="' . $this->plugin_slug . '_settings[testing-toggle]" %s />Enable testing account.',
            checked ( isset($this->options['testing-toggle']), true, false )
        );
    }




    public function debuggingToggle_callback() {
        printf(
            '<input type="checkbox" id="debugging-toggle" name="' . $this->plugin_slug . '_settings[debugging-toggle]" %s />Enable debugging output where shortcode is used.',
            checked ( isset($this->options['debugging-toggle']), true, false )
        );
    }




    public function channelID_testing_callback() {
        printf(
            '<input type="text" id="channelID-testing" name="' . $this->plugin_slug . '_settings[channelID-testing]" value="%s" size="60" maxlength="24" />',
            isset( $this->options['channelID-testing'] ) ? esc_attr( $this->options['channelID-testing']) : ''
        );
    }




    public function apiKey_testing_callback() {
        printf(
            '<input type="text" id="apiKey-testing" name="' . $this->plugin_slug .'_settings[apiKey-testing]" value="%s" size="60" maxlength="39" />',
            isset( $this->options['apiKey-testing'] ) ? esc_attr( $this->options['apiKey-testing']) : ''
        );
    }





    /**************************************************
     ************** FRONT END *************************
     *************************************************/

    public function shortcode() {
        printf('<div class="wunrav-youtube-embed-outter" style="display:none;"><div class="wunrav-youtube-embed-wrapper"><iframe id="wunrav-youtube-embed-iframe" class="wunrav-youtube-embed-iframe" width="%s" height="%s" src="%s" allowfullscreen></iframe></div></div>',
            $this->embed_width, // iframe width
            $this->embed_height, // iframe height
            'https://youtube.com/embed/' . $this->live_video_id . '?autoplay=1&color=white'
        );

        // TODO add the off air message to WP admin options
        echo '<div id="wunrav-youtube-embed-offair">';
        echo '<h3>We aren\'t live quite yet.</h3>';
        echo '<h4>As soon as we start, our live stream will appear.</h4>';
        echo '</div>';

        echo $this->debugging();
    }




    /*
     * Determines if testing is enabled and which level of testing info to display
     *
     * returns int $testing_level The level of testing based on wp-admin settings
     */
    public function is_testing() {
        if ( isset($this->options['testing-toggle']) && isset($this->options['debugging-toggle']) ) {
            $testing_level = 2;
        } elseif ( isset($this->options['testing-toggle']) ) {
            $testing_leve = 1;
        } else {
            $testing_level = 0;
        }

        return $testing_level;
    }




    /*
     * Outputs variables helpful for debugging
     *
     * returns string $out HTML with readable debuggging info
     */
    public function debugging() {
        if ( $this->is_testing() == 2 ) {
            $cached_response = get_transient( 'wunrav_youtube_live_embed' );

            $out  = '<br /><strong>##################################################<br />';
            $out .= ' DEBUGGING<br />';
            $out .= '##################################################<br /></strong>';
            $out .= '<pre>';
            $out .= print_r($this->options, true);
            $out .= '</pre>';
            $out .= '<br /><strong>The query URL</strong><br />';
            $out .= $this->query_string . '<br />';
            if ( false !== $cached_response ) {
                $out .= '<strong>Transient Contents</strong><br /';
                $out .= '<pre>';
                $out .= print_r( $cached_response, true );
                $out .= '</pre>';
            } else {
                $out .= '<strong>No transient exists</strong><br />';
            }
            $out .= '<br /><strong>Direct API Call</strong><br />';
            $out .= '<pre>';
            // query the API for raw response
            $curl = curl_init();
            curl_setopt( $curl, CURLOPT_URL, $this->query_string );
            curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
            $response = json_decode( curl_exec( $curl ) );
            curl_close( $curl );
            $out .= print_r( $response, true );
            $out .= '</pre>'; 
            $out .= '<strong>';
            $out .= '##################################################<br />';
            $out .= ' END DEBUGGING<br />';
            $out .= '##################################################<br /></strong>';
            
            return $out;
        } else {
            return;
        }
    }




    /*
     * Set the channel ID based on wp-admin settings
     *
     * return string $channel_id The channel ID
     */
    public function get_channel_id() {
        if ( $this->is_testing() >= 1 ) {
            $channel_id = $this->options['channelID-testing'];
        } else {
            $channel_id = $this->options['channelID'];
        }

        return $channel_id;
    }

    


    /*
     * Build the query string based on wp-admin settings and schedule cron for next run
     *
     * return void
     */
    public function query_it() {
        // build string for search terms
        if ( null !== $this->options['filterList'] && $this->options['filterType'] == 'Whitelist' || $this->options['filterType'] == 'Exact' ) {
            $q = $this->options['filterList'];
        } else {
            $q = null;
        }

        $query_data = array (
            "part" => 'id,snippet',
            //"channelId" => $this->get_channel_id(), 
            "channelId" => ( $this->options['filterType'] == 'Exact' ? null : $this->get_channel_id() ), // TODO YT API WORKAROUND ... always use the channel ID when API begins working again
            "eventType" => ( $this->options['filterType'] == 'Exact' ? null : 'live' ),
            "type" => 'video',
            "maxResults" => ( $this->options['filterType'] == 'Exact' ? '30' : '20' ), // TODO YT API WORKAROUND. Change 30 to 1 when able
            "order" => 'date',
            "q" => $q,
            "key" => ( 1 <= $this->is_testing() ? $this->options['apiKey-testing'] : $this->options['apiKey'] ),
        );
        $get_query = http_build_query( $query_data ); // transform array into URL query
        $this->query_string = 'https://www.googleapis.com/youtube/v3/search?' . $get_query;

        if ( ! wp_next_scheduled( 'wunrav-youtube-hook' ) ) {
            wp_schedule_event( time(), 'wunrav-90seconds', 'wunrav-youtube-hook' );
        }

        add_action( 'wunrav-youtube-hook', array( $this, 'do_wp_cron' ) );
    }



    /*
     * Schedules the cron to run when wp-cron.php is run
     */
    public function add_wp_cron_schedule() {
        $schedules['wunrav-90seconds'] = array(
            'interval' => 2,
            'display' => __( 'Every 2 seconds' ),
        );

        return $schedules;
    }




    /*
     * Run when wp-cron.php is run. Check the transient, compare the etag store a new transient if needed.
     */
    public function do_wp_cron() {
        date_default_timezone_set( 'America/Chicago' );
        $right_now = date( 'Hi' );
        echo $right_now . '<br />';

        //if ( $right_now <= $start_time || $right_now >= $end_time ) {
        if ( intval( $right_now ) < 2145 || intval( $right_now ) > 2300 ) {
            echo 'Not time yet';
            return;
        }

        $cached_response = get_transient( 'wunrav_youtube_live_embed' );

        // build the header for curl
        $http_header[] = 'Accept: application/json';
        if ( isset( $cached_response['etag'] ) ) {
            $http_header[] = 'If-None-Match: ' . $cached_response['etag'];
        }

        // query the API
        $curl = curl_init();
        curl_setopt( $curl, CURLOPT_URL, $this->query_string );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $curl, CURLOPT_HTTPHEADER, $http_header );
        $http_code = curl_getinfo( $curl, CURLINFO_HTTP_CODE );
        $error = curl_errno( $curl );
        $response = (array) json_decode( curl_exec( $curl ) );
        curl_close( $curl );

        // if the transient etag matches or there is an error then no need to do any more
        if ( $http_code == '304' || ! empty( $error ) || array_key_exists( 'error', $response ) ) {
            return;
        }

        $filter_list = ( isset( $this->options['filterList'] ) ? explode( PHP_EOL, $this->options['filterList'] ) : false );

        // handle the exact match
        // TODO YT API WORKAROUND LIVE... this will need to be reworked when/if the API begins working again
        if ( $this->options['filterType'] == 'Exact' && false !== $filter_list ){
            $exact_title = $filter_list[0];
            $exact_channel_id = $this->get_channel_id();

            foreach ( $response['items'] as $video => $data ) {
                if ( $data['snippet']['title'] !== $exact_title || $data['snippet']['channelId'] !== $exact_channel_id ) {
                    array_shift( $response['items'] );
                } else {
                    break;
                }
            }
        }

        // use the blacklist to modify the JSON
        // TODO add the keywords to the query instead of modifying the response (i.e. add to $q -term)
        if ( $this->options['filterType'] == 'Blacklist' && false !== $filter_list ){
            $filter_list_regex = '/';

            // build a blackli st and remove them from the results
            // take the filterList as an array and make a regex
            for ( $i = 0; $i < count( $filter_list ); $i++ ) {
                $filter_list_regex .= '(' . trim( $filter_list[$i] ) . ')' . ( $i < count( $filter_list )-1 ? '|' : '/' );
            } 

            foreach ( $response['items'] as $video => $data ) {
                // only need to do this once if the first one is NOT a match
                if ( preg_match( $filter_list_regex, $data['snippet']['title'] ) ) {
                    array_shift( $response['items'] );
                } else {
                    break;
                }
            }
        }

        set_transient( 'wunrav_youtube_live_embed', $response, 60 );
    }




    /*
     * Build the slideout / alert that is displayed when a live stream is detected
     *
     * return string $out The HTML of the slideout / alert
     */
    public function alert() {
        global $post;
        $post_content = ( isset( $post->post_content ) ? $post->content : false );

        if ( ! has_shortcode( $post_content, 'youtube-live' ) ) {
            $out  = '<input type="checkbox" id="slideout-button" name="slideout-button" style="display:none;">';
            $out .= '<div class="live-feed-slideout" id="wunrav-youtube-embed-slideout" ' . ( $this->is_testing() >= 1 ? '' : 'style="display:none;"' ) . '>';
            $out .= '<div class="slideout-content-wrap">';
            $out .= '<div class="slideout-content">';
            $out .= '<h2>' . $this->options['alertTitle'] . '</h2>';
            $out .= '<p>' . $this->options['alertMsg'] . '</p>';
            $out .= '<a href="' . $this->options['alertBtnURL'] . '"><h4 class="lptv-blue-button-big">' . $this->options['alertBtn'] . '</h4></a>';
            $out .= '</div>';
            $out .= '</div>';
            $out .= '<label for="slideout-button" id="slideout-trigger" class="slideout-trigger onAir"><img src="'. plugins_url('images/arrow-triangle.png', __FILE__) .'" /><br />' . implode( "<br />", str_split("LIVE") ) . '</label>';
            $out .= '</div>';

            echo $out;
        } else {
            return;
        }
    }




    /*
     * Enqueue styles and javascript
     */
    public function load_scripts() {
        wp_enqueue_style('wunrav-youtube-live-embed-style', plugins_url('wunrav-youtube-live-embed/includes/stylesheets/css/style.css'), __FILE__);
        wp_enqueue_script('wunrav-youtube-live-embed-cookie', plugins_url('wunrav-youtube-live-embed/includes/live-feed-cookie.js'), __FILE__);
        wp_enqueue_script('wunrav-youtube-live-embed-clientside', plugins_url('wunrav-youtube-live-embed/includes/clientside.js'), __FILE__);
    }




    /*
     * @param string $message The error message to be displayed
     *
     * @param string $type The builtin WP CSS class to use (notice-error, notice-warning, notice-success, notice-info)
     *
     * @return string Displays an admin notice
     */
    public function admin_notice( $message = null, $type = 'notice-error' ) {
        if ( ! is_admin() || empty( $message ) ) {
            return;
        }

        $err = '<div class="' . $type . ' notice"><p>' . $message . '</p></div>';

        return $err;
    }



    /*
     * Remove the cron when the plugin is deactivated
     *
     * return void
     */
    public function deactivate() {
        // remove the wp-cron entry
        wp_clear_scheduled_hook( 'wunrav-youtube-hook' );
    }

}
